set __toaster_color_orange "#D8AA32"
set __toaster_color_blue "#ffcc20"
set __toaster_color_green "#a9b73c"
set __toaster_color_yellow "#ffe42f"
set __toaster_color_pink "#ffc91c"
set __toaster_color_red "#CF9722"
set __toaster_color_grey "#e8d499"
set __toaster_color_white "#ffffc8"
set __toaster_color_purple "#ddd251"
set __toaster_color_lilac "#9F984A"
set __toaster_color_grey_dark "#121715"

set __toaster_color_yellow_bold "#ffe42f --bold"
set __toaster_color_grey_bold "#e8d499 --bold"

function __toaster_color_echo
  set_color (echo $argv[1] | cut -d '#' -f 2)
  if test (count $argv) -eq 2
    echo -n $argv[2]
  end
end

function __toaster_current_folder
  if test $PWD = '/'
    echo -n '/'
  else
    echo -n $PWD | grep -o -E '[^\/]+$'
  end
end

function __toaster_git_status_codes
  echo (git status --porcelain ^/dev/null | sed -E 's/(^.{3}).*/\1/' | tr -d ' \n')
end

function __toaster_git_branch_name
  echo (git rev-parse --abbrev-ref HEAD ^/dev/null)
end

function __toaster_rainbow
  if echo $argv[1] | grep -q -e $argv[3]
    __toaster_color_echo $argv[2] "彡ミ"
  end
end

function __toaster_git_status_icons
  set -l git_status (__toaster_git_status_codes)

  __toaster_rainbow $git_status $__toaster_color_pink 'D'
  __toaster_rainbow $git_status $__toaster_color_red 'D'
  __toaster_rainbow $git_status $__toaster_color_orange 'R'
  __toaster_rainbow $git_status $__toaster_color_white 'C'
  __toaster_rainbow $git_status $__toaster_color_green 'A'
  __toaster_rainbow $git_status $__toaster_color_blue 'U'
  __toaster_rainbow $git_status $__toaster_color_lilac 'M'
  __toaster_rainbow $git_status $__toaster_color_grey '?'
end

function __toaster_git_status
  # In git
  if test -n (__toaster_git_branch_name)

    #__toaster_color_echo $__toaster_color_blue " git"
    __toaster_color_echo $__toaster_color_grey " @ "
    __toaster_color_echo $__toaster_color_purple (__toaster_git_branch_name)

    if test -n (__toaster_git_status_codes)
      __toaster_color_echo $__toaster_color_red ' ●'
      __toaster_color_echo $__toaster_color_yellow ' [^._.^]ﾉ'
      __toaster_git_status_icons
    else
      __toaster_color_echo $__toaster_color_green ' ○'
    end
  end
end

function fish_prompt
  __toaster_color_echo $__toaster_color_yellow "•" 
  __toaster_color_echo $__toaster_color_red "•" 
  __toaster_color_echo $__toaster_color_lilac "•" 
  __toaster_color_echo $__toaster_color_blue "• " 
  __toaster_color_echo $__toaster_color_yellow (__toaster_current_folder)
  __toaster_git_status
  echo
  __toaster_color_echo $__toaster_color_grey "∷ "
end
